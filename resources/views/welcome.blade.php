<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script
            src="https://code.jquery.com/jquery-3.6.0.js"
            integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
            crossorigin="anonymous">
        </script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
<!-- JavaScript Bundle with Popper -->
    </head>
    <body class="antialiased">
        <div class="container">
        <div id="accordion">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#getCards" aria-expanded="true" aria-controls="getCards">
                            گرفتن کارت ها
                        </button>
                    </h5>
                </div>
          
              <div id="getCards" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body col-12">
                        <div class="form-group">
                            <label for="cardUser">صاحب کارت</label>
                            <input type="text" class="form-control" id="cardUser" placeholder="اختیاری">
                        </div>

                        <div class="form-group">
                            <label for="cardUser">وضعیت</label>
                            <select class="form-control">
                                <option value="">اتخاب وضعیت</option>
                                <option value="0">درحال انجام</option>
                                <option value="1">انجام شده</option>
                            </select>
                        </div>

                    </div>
                    <button id="btnGetCards" type="submit" class="btn btn-primary mr-4">درخواست</button>
                </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingTwo">
                <h5 class="mb-0">
                  <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#createCard" aria-expanded="false" aria-controls="createCard">
                    ساخت کارت جدید
                  </button>
                </h5>
              </div>
              <div id="createCard" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card-body">
                        <div class="form-group">
                            <label for="cardUserCreate">نام صاحب کارت</label>
                            <input type="text" class="form-control" id="cardUserCreate">
                        </div>
                        <div class="form-group">
                            <label for="cardTitleCreate">عنوان</label>
                            <input type="text" class="form-control" id="cardTitleCreate">
                        </div>
                        <div class="form-group">
                            <label for="cardDescriptionCreate">توضیحات</label>
                            <textarea type="text" class="form-control" id="cardDescriptionCreate" placeholder="اختیاری"></textarea>
                        </div>
                        <button id="btnCreateCard" type="submit" class="btn btn-primary m-4">بساز</button>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingThree">
                <h5 class="mb-0">
                  <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#editCard" aria-expanded="false" aria-controls="editCard">
                    ویرایش کارت
                  </button>
                </h5>
              </div>
              <div id="editCard" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                    <div class="form-group">
                        <label for="cardIdEdit">شماره کارت</label>
                        <input type="text" class="form-control" id="cardIdEdit">
                    </div>
                    <div class="form-group">
                        <label for="cardUserEdit">نام صاحب کارت</label>
                        <input type="text" class="form-control" id="cardUserEdit">
                    </div>
                    <div class="form-group">
                        <label for="cardTitleEdit">عنوان</label>
                        <input type="text" class="form-control" id="cardTitleEdit">
                    </div>
                    <div class="form-group">
                        <label for="cardDescriptionEdit">توضیحات</label>
                        <textarea type="text" class="form-control" id="cardDescriptionEdit" placeholder="اختیاری"></textarea>
                    </div>
                    <button id="btnEditCard" type="submit" class="btn btn-primary m-4">ویرایش</button>
            </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingThree">
                <h5 class="mb-0">
                  <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#deleteCard" aria-expanded="false" aria-controls="deleteCard">
                    حذف کارت
                  </button>
                </h5>
              </div>
              <div id="deleteCard" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                    <div class="form-group">
                        <label for="cardIdDelete">شماره کارت</label>
                        <input type="text" class="form-control" id="cardIdDelete">
                    </div>
                    <button id="btnDeleteCard" type="submit" class="btn btn-primary m-4">حذف</button>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingThree">
                <h5 class="mb-0">
                  <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#changeCardStatus" aria-expanded="false" aria-controls="changeCardStatus">
                    تغییر وضعیت کارت
                  </button>
                </h5>
              </div>
              <div id="changeCardStatus" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                    <div class="form-group">
                        <label for="cardIdChange">شماره کارت</label>
                        <input type="text" class="form-control" id="cardIdChange">
                    </div>
                    <button id="btnChange" type="submit" class="btn btn-primary m-4">تغییر وضعیت</button>
                </div>
              </div>
            </div>
        </div>
        <p id="result" dir="ltr">

        </p>
        </div>
        <script>
            $(document).ready(function(){
                $("#btnGetCards").click(function(){
                    $('#result').text("");
                    var carduser = $("#cardUser").val();
                    console.log(carduser);
                    $.ajax({
                        url: 'api/cards',
                        type: 'get',
                        data: {
                            user_name: carduser,
                        },
                        dataType: 'json',
                        success: function (response) {
                            $.each(response, function (index, value) {
                                str = JSON.stringify(value);
                                $('#result').append(str)
                            });
                        },

                    });
                });
                $("#btnCreateCard").click(function(){
                    $('#result').text("");
                    var cardUser = $("#cardUserCreate").val();
                    var cardTitle = $("#cardTitleCreate").val();
                    var cardescription = $("#cardDescriptionCreate").val();
                    
                    $.ajax({
                        url: 'api/cards',
                        type: 'post',
                        data: {
                            user_name: cardUser,
                            title: cardTitle,
                            description: cardescription,
                        },
                        dataType: 'json',
                        success: function (response) {
                            alert(response.message);
                        },
                        error : function(xhr) {
                            alert(xhr.responseJSON.message);
                        }
                    });
                });
                $("#btnEditCard").click(function(){
                    $('#result').text("");
                    var cardId = $("#cardIdEdit").val();
                    var cardUser = $("#cardUserEdit").val();
                    var cardTitle = $("#cardTitleEdit").val();
                    var cardescription = $("#cardDescriptionEdit").val();
                    
                    $.ajax({
                        url: 'api/cards/'+cardId,
                        type: 'PUT',
                        data: {
                            user_name: cardUser,
                            title: cardTitle,
                            description: cardescription,
                        },
                        method: 'PUT',
                        dataType: 'json',
                        success: function (response) {
                            alert(response.message);
                        },
                        error : function(xhr) {
                            alert(xhr.responseJSON.message);
                        }
                    });
                });
                $("#btnDeleteCard").click(function(){
                    $('#result').text("");
                    var cardId = $("#cardIdDelete").val();
                    
                    $.ajax({
                        url: 'api/cards/'+cardId,
                        type: 'DELETE',
                        method: 'DELETE',
                        dataType: 'json',
                        success: function (response) {
                            alert(response.message);
                        },
                        error : function(xhr) {
                            alert(xhr.responseJSON.message);
                        }
                    });
                });
                $("#btnChange").click(function(){
                    $('#result').text("");
                    var cardId = $("#cardIdChange").val();
                    
                    $.ajax({
                        url: 'api/cards/toggleChange/'+cardId,
                        type: 'GET',
                        method: 'GET',
                        dataType: 'json',
                        success: function (response) {
                            alert(response.message);
                        },
                        error : function(xhr) {
                            alert(xhr.responseJSON.message);
                        }
                    });
                });
            });
        </script>
    </body>
</html>
