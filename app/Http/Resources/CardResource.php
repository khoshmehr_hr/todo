<?php

namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;
use Morilog\Jalali\CalendarUtils;

class CardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_name' => $this->user_name,
            'title' => $this->title,
            'description' => $this->description,
            'status' => $this->get_status(),
            'created_at' => CalendarUtils::strftime('Y-m-d H:i', strtotime($this->created_at)),
            'updated_at' => CalendarUtils::strftime('Y-m-d H:i', strtotime($this->updated_at)),
        ];
    }
}
