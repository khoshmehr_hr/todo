<?php

namespace App\Http\Controllers;

use App\Http\Resources\CardResource;
use App\Models\Card;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CardController extends Controller
{

    public function index(Request $request)
    {
        $cards= Card::orderByDesc('id')->get();

        if($request->user_name) {
            $cards = $cards->where('user_name', $request->user_name);
        }
        if($request->status) {
            $cards = $cards->where('status', $request->status);
        }

        return response()->json(CardResource::collection($cards), 200);
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'description' => 'string',
            'user_name' => 'required|string',
        ]);
        
        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->all()], 400);
        }
        
        Card::create($request->all());

        return response()->json(['message' => 'کارت اضافه شد.'], 200);
    }


    public function update(Request $request, Card $card)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'string',
            'description' => 'string',
            'user_name' => 'string',
        ]);

        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->all()], 400);
        }
        
        $card->update($request->all());

        return response()->json(['message' => 'کارت به روز رسانی شد.'], 200);
    }


    public function destroy(Card $card)
    {
        $card->delete();

        return response()->json(['message' => 'کارت حذف شد.'], 200);
    }

    public function toggleChange(Card $card)
    {
        $card->status = !$card->status;
        $card->save();

        return response()->json(['message' => 'کارت در حالت '.$card->get_status().' قرار گرفت'], 200);
    }
}
