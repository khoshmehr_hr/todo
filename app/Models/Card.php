<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    use HasFactory;

    protected $fillable= [
        'title',
        'description',
        'user_name',
        'status',
    ];

    public function get_status() {

        $statuses = [
            0 => 'در حال انجام',
            1 => 'انجام شده',
        ];

        return $statuses[$this->status];
    }
}
